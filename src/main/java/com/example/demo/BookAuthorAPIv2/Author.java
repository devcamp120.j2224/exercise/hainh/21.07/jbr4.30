package com.example.demo.BookAuthorAPIv2;

public class Author {
    public static final int author2 = 0;
    public String name ;
    public char gender ;
    public String email ;
    

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public char getGender() {
        return gender;
    }


    public void setGender(char gender) {
        this.gender = gender;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public Author(Author authors) {
    }


    public Author(String name, char gender, String email) {
        this.name = name;
        this.gender = gender;
        this.email = email;
    }


    @Override
    public String toString() {
        return "Author [email=" + email + ", gender=" + gender + ", name=" + name + "]";
    }


    
}
