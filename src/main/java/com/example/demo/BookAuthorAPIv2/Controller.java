package com.example.demo.BookAuthorAPIv2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/book2")
    public ArrayList<Book> getListBook(){
        ArrayList<Book> books = new ArrayList<>();

        Author author1 = new Author("Quyên" , 'F' ,"caiquanque1111@yahoo.com");
        Author author2 = new Author("Quỳnh" , 'F' ,"caiquanque2222@yahoo.com");
        Author author3 = new Author("Quang" , 'M' ,"caiquanque3333@yahoo.com");
        Author author4 = new Author("Hải" , 'M' ,"caiquanque4444@yahoo.com");
        Author author5 = new Author("Thạnh" , 'M' ,"caiquanque5555@yahoo.com");
        Author author6 = new Author("Giang" , 'F' ,"caiquanque6666yahoo.com");

    
        ArrayList<Author> authorList1 = new ArrayList<Author>();
        ArrayList<Author> authorList2 = new ArrayList<Author>();
        ArrayList<Author> authorList3 = new ArrayList<Author>();

        authorList1.add(author1);
        authorList1.add(author2);
        authorList2.add(author3);
        authorList2.add(author4);
        authorList3.add(author5);
        authorList3.add(author6);


        Book book1 = new Book("Giang" , authorList1 ,344.00,21321);
        Book book2 = new Book("Thạnh" , authorList2,12312.00,29999);
        Book book3 = new Book("Ninh" , authorList3,42342.00,28888);  

  
        books.add(book1);
        books.add(book2);
        books.add(book3);

        return books ;
    }
}
